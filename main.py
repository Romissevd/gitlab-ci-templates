def func():
    print('Hello world!')


def func2(a: int, b: int):
    return a + b


if __name__ == '__main__':
    func()
    func2(1, 2)
